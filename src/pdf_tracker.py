from PyPDF2 import PdfReader, PdfWriter
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
from io import BytesIO
import uuid

# Tracking pixel URL base
TRACKING_URL_BASE = "https://rodderscode.co.uk/pixel.jpg"

def _create_tracking_pdf(company_name):
    """Creates a temporary PDF with a tracking image reference."""
    buffer = BytesIO()
    c = canvas.Canvas(buffer, pagesize=letter)
    
    # Generate unique identifier for tracking
    tracking_id = str(uuid.uuid4())
    tracking_url = f"{TRACKING_URL_BASE}?user={company_name}"
    
    # Add an annotation that references the external image
    # This creates a link annotation that's invisible (1x1) and positioned off-page
    c.linkURL(tracking_url, (0, 0, 1, 1), relative=0)
    
    # Draw something (even invisible) to ensure the page is created
    c.setFillColorRGB(1, 1, 1, 0)  # Transparent white
    c.rect(0, 0, 1, 1, fill=True)
    
    c.save()
    buffer.seek(0)
    return buffer

def add_tracking_to_pdf(input_pdf_path, output_pdf_path, company_name):
    """Merges tracking reference into an existing PDF."""
    # Load original PDF
    reader = PdfReader(input_pdf_path)
    writer = PdfWriter()

    # Create tracking PDF
    tracking_pdf = _create_tracking_pdf(company_name)
    tracking_reader = PdfReader(tracking_pdf)
    
    # Ensure tracking PDF has pages before proceeding
    if len(tracking_reader.pages) == 0:
        raise ValueError("Tracking PDF has no pages")

    for page in reader.pages:
        page.merge_page(tracking_reader.pages[0])  # Merge tracking PDF onto each page
        writer.add_page(page)

    # Save new PDF
    with open(output_pdf_path, "wb") as f:
        writer.write(f)

# Example usage
add_tracking_to_pdf("saved/resume-2025.pdf", "saved/resume-2025-tracked.pdf", "calling_main_company")
