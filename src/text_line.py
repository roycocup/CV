from . import helpers

def text_line(text, bullet=False, space_below=True):
    output = helpers.get_n_spaces(2) + ('* ' if bullet else '') + text
    if space_below:
        output += "\n"
    return output 