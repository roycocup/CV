from flask import send_file, render_template
from datetime import datetime
from .cv_generator import CVGenerator
from .pdf_generator import PDFGenerator
from flask import request


def setup_routes(app):
    
    fileName = f'resume-{datetime.now().year}'
    filePath = f'saved/{fileName}'
    
    @app.route('/')
    def index():
        return render_template('home.html')

    @app.route('/create/text')
    def create_text_cv():
        cv_gen = CVGenerator()
        cv_content = cv_gen.generate_cv()
        cv_content = cv_content.replace('<<<PAGE_BREAK>>>', '')
        
        with open(filePath + '.txt', 'w') as f:
            f.write(cv_content)
            
        return render_template('home.html')

    
    @app.route('/create/pdf')
    def create_pdf():
        company_name = request.args.get('company_name', 'rodderscode-pdf')
        pdf_generator = PDFGenerator(
            cv_content=CVGenerator().generate_cv(),
            current_year=datetime.now().year,
            company_name=company_name
        )
        pdf_generator.generate(fileName + '.pdf')
        
        return render_template('home.html')


    @app.route('/download/text')
    def download_text():
        create_text_cv()
        
        return send_file(
            filePath + '.txt',
            as_attachment=True,
            download_name=fileName + '.txt',
            mimetype='text/plain'
        )

    @app.route('/download/pdf')
    def download_pdf():
        create_pdf()
        
        return send_file(
            filePath + '.pdf',
            as_attachment=True,
            download_name=fileName + '.pdf',
            mimetype='application/pdf'
        ) 