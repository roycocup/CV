import math
from dataclasses import dataclass

@dataclass
class Vector:
    x: float
    y: float
    
    def sub(self, other):
        return Vector(self.x - other.x, self.y - other.y)
    
    def mag(self):
        return math.sqrt(abs(self.x)) + math.sqrt(abs(self.y))

class CircleGraph:
    def __init__(self, data):
        self.r = data['radius']
        self.width = 0
        self.height = 0
        self.matrix = {}
        self.create_matrix()
    
    def create_matrix(self):
        for y in range(self.r * 2):
            for x in range(self.r * 4):
                self.matrix[Vector(x, y)] = {'data': ' '}
                self.width = max(self.width, x)
            self.height = max(self.height, y)
    
    def center(self):
        return Vector(math.ceil(self.width / 2), math.ceil(self.height / 2))
    
    def get_value(self, key):
        return self.matrix.get(self.get_key(key))
    
    def get_key(self, lookup):
        for key in self.matrix:
            if key.x == lookup.x and key.y == lookup.y:
                return key
        return None
    
    def draw(self):
        center = self.center()
        for key in list(self.matrix.keys()):
            dist = key.sub(center).mag()
            if dist < 4:
                self.matrix[key] = {'data': 'X'}
        
        output = ''
        for key in sorted(self.matrix.keys(), key=lambda v: (v.y, v.x)):
            value = self.get_value(key)
            output += value['data']
            if key.x >= self.width:
                output += "\n"
        return output 