╔══════════════════╗
║ PERSONAL DETAILS ║
╚══════════════════╝
  Rodrigo Dias

  rodrigo@rodderscode.co.uk

  Remote

╔═══════════════════════╗
║ CYBERSECURITY SKILLS  ║
╚═══════════════════════╝
 * Penetration Testing        ░░░▒▒▒▓▓▓
 * Cloud Security             ░░░▒▒▒▓▓▓
 * Vulnerability Assessment   ░░░▒▒▒▓▓
 * Secure Software Development░░░▒▒▒▓▓
 * Linux Security             ░░░▒▒▒▓
 * Incident Response          ░░░▒▒▒▓

╔════════════════════╗
║ PROGRAMMING SKILLS ║
╚════════════════════╝
 * Python                 ░░░▒▒▒▓▓▓
 * PHP                    ░░░▒▒▒▓▓
 * Bash/Shell Scripting   ░░░▒▒▒▓

╔═════════════════════╗
║ CLOUD & TOOLS USED  ║
╚═════════════════════╝
 * AWS Security Hub        ░░░▒▒▒▓▓▓
 * Docker & Kubernetes     ░░░▒▒▒▓▓
 * Git                     ░░░▒▒▒▓
 * OWASP ZAP               ░░░▒▒▒▓
 * Metasploit Framework    ░░░▒▒▒▓
 * Hack The Box            ░░░▒▒▒▓

╔═════════════════╗
║ CERTIFICATIONS  ║
╚═════════════════╝
 * CompTIA Security+ Completed (2024)
 * eLearnSecurity Junior Penetration Tester (eJPT) Completed (2024)
 * AWS Certified Security – Specialty  Completed (2024)
 * Certified Application Security Engineer (CASE) Completed (2024)

╔═════════════════╗
║ WORK EXPERIENCE ║
╚═════════════════╝
+-------------------------+-----------------------+----------+---------------------+
| Title                   | Company               | Location | Dates               |
+-------------------------+-----------------------+----------+---------------------+
| Cybersecurity Engineer  | SecureTech Solutions | Remote   | Aug 2024 - Present  |
| Senior Engineer         | Above Surveying       | Remote   | Feb 2023 - Aug 2024 |
| Senior Engineer         | Pooldata              | Remote   | Sep 2022 - Jan 2023 |
| Senior Engineer         | XIM - Lifelight       | Remote   | Oct 2021 - Sep 2022 |
| (See prior roles below) |                       |          |                     |
+-------------------------+-----------------------+----------+---------------------+

╔════════════════════╗
║ CYBERSECURITY WORK ║
╚════════════════════╝
  * At SecureTech Solutions:
    - Conducted penetration testing using tools like Metasploit and OWASP ZAP.
    - Identified and remediated vulnerabilities in web applications and cloud infrastructure.
    - Built incident response playbooks and handled real-world phishing attack mitigation.

  * At Above Surveying:
    - Transitioned into a hybrid role by securing backend systems and integrating AWS Security Hub for compliance.
    - Conducted database security audits and implemented encryption for sensitive data.

  * Personal Projects:
    - Participated in Hack The Box challenges, achieving top 20% ranking in regional leaderboards.
    - Built and maintained a GitHub portfolio of application security audits and remediation scripts.

╔══════════════════╗
║ OVERVIEW         ║
╚══════════════════╝
  * 17 years of software engineering experience transitioning into cybersecurity.

  * Extensive knowledge of backend system hardening, cloud security, and application testing.

  * Well-versed in building smart, secure applications with robust API and third-party connections.

  * Experienced in balancing the needs of innovative engineering with security compliance.

╔══════════════════╗
║ PERSONAL NOTES   ║
╚══════════════════╝
  * Honest, reliable, and focused on delivering high-value, secure solutions.

  * Enjoy reviewing team workflows and processes to align with business and security goals.

  * Easy to reach via email (rodrigo@rodderscode.co.uk) as my phone is typically muted.

╔══════════════════╗
║ ADDITIONAL INFO ║
╚══════════════════╝
  This CV is a transition snapshot showcasing current cybersecurity expertise alongside a long-standing engineering career.

