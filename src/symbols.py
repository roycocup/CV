def box():
    return ['╔', '═', '╗', '║', '╚', '╝']

def semiblocks():
    return ['258F', '258E', '258D', '258C', '258B', '258A', '2589', '2588',
            '2503', '2501', '2501', '250F', '2507', '251B']

def filled_blocks():
    return ['2591', '2592', '2593']

def misc():
    return {
        'heart': 2665,
        'filled_star': 2605,
        'outline_star': 2606,
        'umbrella': 2602,
        'cross': '+',
        'dash': '-',
        'side': '|'
    } 