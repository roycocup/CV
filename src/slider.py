def slider(left_label, right_label, skew, length=20):
    """
    Draws an ASCII slider graph with a diamond (◇) indicating the skew position.
    
    :param left_label: Label for the left end of the scale
    :param right_label: Label for the right end of the scale
    :param skew: Value between 0 (left) and 1 (right) indicating the diamond's position
    :param length: Total length of the scale (default is 20 characters)
    """
    skew = max(0, min(1, skew))  # Ensure skew is within bounds
    position = round(skew * (length - 1))  # Calculate position for the diamond
    
    # Generate the scale line with a diamond at the calculated position
    scale = ["-"] * length
    scale[position] = "|"  # Middle marker
    scale_str = "[" + "".join(scale) + "]"
    
    # Generate the marker line with the diamond in the same position
    marker = [" "] * length
    marker[position] = "◇"
    marker_str = " " + "".join(marker) + " "
    
    # Print the assembled slider
    return marker_str + "\n" + scale_str + "\n" + f"{left_label:<{length//2}} {right_label:>{length//2}}"