from . import symbols
from . import helpers

def boxed_title(title, page_break=False):
    output = []
    if page_break:
        output.append("<<<PAGE_BREAK>>>")  # Special marker for PDF generation
    output.append("┌" + "─" * (len(title) + 2) + "┐")
    output.append("│ " + title + " │")
    output.append("└" + "─" * (len(title) + 2) + "┘")
    return "\n".join(output) 