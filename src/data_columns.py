from . import symbols
from . import helpers

def get_info_for_columns(col_data, num_columns):
    columns_info = []
    for i in range(num_columns):
        col_info = [item for item in col_data if item['col'] == i]
        char_count_list = [item['char_count'] for item in col_info]
        max_chars = max(char_count_list)
        columns_info.append({'col': i, 'max_chars': max_chars})
    return columns_info

def division(columns_info, cell_padding):
    misc_chars = symbols.misc()
    output = ""
    for i in range(len(columns_info)):
        output += misc_chars['cross']
        n = columns_info[i]['max_chars'] + cell_padding
        output += helpers.get_n_symbols(n, misc_chars['dash'])
    output += misc_chars['cross'] + "\n"
    return output

def create_row(row, col_data, columns_info, text_margin, cell_padding, include_division=False):
    misc_chars = symbols.misc()
    output = misc_chars['side']
    
    row_data = [item for item in col_data if item['row'] == row]
    for element in row_data:
        output += helpers.get_n_symbols(text_margin, ' ')
        output += element['data']
        output += helpers.get_n_spaces(columns_info[element['col']]['max_chars'] - element['char_count'] + text_margin)
        output += misc_chars['side']
    
    output += "\n"
    if include_division:
        output += division(columns_info, cell_padding)
    
    return output

def data_columns(data):
    cell_padding = 2
    text_margin = 1
    
    col_data = []
    num_columns = len(data[0])
    
    for row, items in enumerate(data):
        for col, item in enumerate(items):
            col_data.append({
                'row': row,
                'col': col,
                'data': item,
                'char_count': len(item)
            })
    
    columns_info = get_info_for_columns(col_data, num_columns)
    
    output = division(columns_info, cell_padding)
    output += create_row(0, col_data, columns_info, text_margin, cell_padding, True)
    
    for row in range(1, len(data)):
        output += create_row(row, col_data, columns_info, text_margin, cell_padding)
    
    output += division(columns_info, cell_padding)
    return output 