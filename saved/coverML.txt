Dear XXX, 

I am an experienced PHP developer who is very keen on making a shift to the ML world. 
Apart from what you can read on my CV I would like to point out that I did a course in ML for certification, I developed some examples and  apps for my own development practice and currently am developing some video games using Python and Lua. 

(https://github.com/roycocup?tab=repositories&language=python)

I am pretty keen on learning new languages and tools and thats precisely one of the reasons why I’m applying for this position. 

Although I might not have the commercial experience using some of the tools you require but I do have about 10 years of experience in the field of software development and am pretty much in the loop for software development best practices and techniques.

I really think an interview could clear up on wether or not I could easily be a good fit within the company and I would be ever so grateful if you could take a moment to consider my CV attached.

Looking forward to hear from you, 
Rod