Dear XXX, 

I am an experienced PHP developer living in London but who is looking for a new position in the Barcelona area. I intend to move as early as June this year but would be available to start remotely in March.

I have only worked commercially with PHP and Go but have an very keen interest in working in other languages, and have done several personal projects with some others (ES6, Python, Java, C#, etc… ) 

You seem to utilise some tools that I may not have experience with which is also why I was even more interested in applying. 

Although I might not have the commercial experience using some of the tools you require I do have about 10 years of experience in the field of software development and am pretty much in the loop for software development best practices and techniques.

I really think an interview could clear up on whether or not I could easily be a good fit within the company and I would be ever so grateful if you could take a moment to consider my CV attached. 

Looking forward to hear from you, 
Rod