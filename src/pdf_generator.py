from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, PageBreak, Image
from reportlab.lib.styles import ParagraphStyle, getSampleStyleSheet
from reportlab.lib.pagesizes import letter, A4
from reportlab.lib.units import mm
from reportlab.lib.pagesizes import inch
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
import os
from .pdf_tracker import add_tracking_to_pdf

class FontManager:
    @staticmethod
    def get_font():
        try:
            pdfmetrics.registerFont(TTFont('DejaVuSansMono', 'DejaVuSansMono.ttf'))
            return 'DejaVuSansMono'
        except:
            return 'Courier'

class PDFGenerator:
    def __init__(self, cv_content, current_year, company_name):
        self.cv_content = cv_content
        self.current_year = current_year
        self.font_name = FontManager.get_font()
        self.company_name = company_name
        
    
    def generate(self, filename):
        pdf_path = f"saved/{filename}"
        doc = SimpleDocTemplate(
            pdf_path,
            pagesize=letter,
            leftMargin=15,
            rightMargin=15,
            topMargin=25,
            bottomMargin=25
        )
        
        # Create custom style based on the monospace font
        styles = getSampleStyleSheet()
        custom_style = ParagraphStyle(
            'CustomStyle',
            parent=styles['Normal'],
            fontName=self.font_name,
            fontSize=10,
            leading=14,
            spaceBefore=0,
            spaceAfter=0,
            firstLineIndent=0,
            leftIndent=0,
            rightIndent=0,
            wordWrap='CJK'
        )
        
        # Convert content to Platypus flowables
        story = []
        for line in self.cv_content.split('\n'):
            if line == "<<<PAGE_BREAK>>>":
                story.append(PageBreak())
                continue
                
            # Replace spaces with non-breaking spaces to preserve formatting
            formatted_line = line.replace(' ', '&nbsp;')
            if formatted_line.strip():  # Only process non-empty lines
                p = Paragraph(formatted_line, custom_style)
                story.append(p)
            else:
                # Add a small vertical space for empty lines
                story.append(Spacer(1, 12))
        
        # Add tracking pixel
        print("Adding tracking pixel")
        add_tracking_to_pdf(pdf_path, "saved/resume-2025-tracked.pdf", self.company_name)
        
        # Build the PDF
        doc.build(story)
        
        return pdf_path