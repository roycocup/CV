import boto3
from pprint import pprint
import json
import os

def get_client():
    bedrock_runtime = boto3.client(
        service_name='bedrock-runtime',
        region_name='us-east-1'
    )
    return bedrock_runtime



def get_text(job_advert, cv_template):
    text = f"""
        You are a professional CV writer specializing in technical roles. Your task is to customize the CV based on the job requirements while maintaining authenticity.

        Job Advertisement:
        ```
        {job_advert}
        ```

        Base CV Template:
        ```
        {cv_template}
        ```

        Instructions:
        1. Analyze the job requirements and identify key skills, technologies, and experiences they're seeking
        2. Reorganize and emphasize relevant experiences from the CV that match these requirements
        3. Maintain the ASCII formatting but ensure readability
        4. For each section, add "**Recommended**" items that would strengthen the application
        5. Use the following skill level indicators consistently:
           ░ (Basic) ▒ (Intermediate) ▓ (Advanced)
        6. Focus on quantifiable achievements and specific technical contributions
        7. Ensure all dates and experiences remain truthful to the original CV

        Format the response as a complete CV, maintaining the ASCII box structure.
        """
    return text


def get_payload(text):
    payload = {
        "messages": [{
            "role": "user",
            "content": [{"text": text}]
            }]
        }
    return payload


def get_response(payload, client):
    response = client.invoke_model(
        modelId='amazon.nova-lite-v1:0', 
        contentType='application/json',
        accept='application/json',
        body=json.dumps(payload)
    )

    return response.get('body').read()

def fetch_ai_response(job_advert, cv_template):
    try:
        text = get_text(job_advert, cv_template)
        payload = get_payload(text)
        response = get_response(payload, get_client())
        
        # Create test_responses directory if it doesn't exist
        os.makedirs("test_responses", exist_ok=True)
        
        with open("test_responses/response.txt", "wb+") as f:
            f.write(response)
    except Exception as e:
        print(f"Error generating CV: {str(e)}")
        raise

def read_response(advert_name):
    with open("test_responses/response.txt", "r") as f:
        response = f.read()
    text_response = json.loads(response)['output']['message']['content'][0]['text']
    with open(f"test_responses/{advert_name}_cv_response.txt", "w") as f:
        f.write(text_response)

if __name__ == "__main__":
    cv_template = open("test_templates/cv_template.txt", "r").read()
    
    job_advert = open("test_adverts/safe_intelligence.txt", "r").read()

    fetch_ai_response(job_advert, cv_template)
    read_response("safe_intelligence")
    print("AI response saved to cv_response.txt")