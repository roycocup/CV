def parse_symbol(symbol):
    return chr(int(symbol, 16))

def get_n_spaces(n):
    return ' ' * n

def get_n_symbols(n, symbol):
    return symbol * n

def get_n_p_symbols(n, symbol):
    return parse_symbol(symbol) * n 