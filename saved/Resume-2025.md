**Rodrigo Dias**  
Email: rodrigo@rodderscode.co.uk | Location: Remote | Website: [rodderscode.co.uk](http://rodderscode.co.uk)  

---

### **Summary**

Senior Software Engineer with **17+ years of experience** in **full-stack development, backend architecture, and cloud computing**. Proven expertise in building **scalable applications, API integrations, and data engineering solutions**. Passionate about **automation, DevOps, and process optimization**. Experienced in leading development teams and introducing **modern engineering practices** like Scrum, Kanban, and Extreme Programming.

- **Overhauled backend infrastructure** and built a **new digital twin system** for the **largest solar panel health database** at Above Surveying.
- **Developed an ETL pipeline** at XIM, enabling **real-time data processing** for scientific research.
- **Introduced data warehousing, Neo4j, and automation** at multiple organizations to improve efficiency.
- Experienced in **remote work** with distributed teams across different time zones.

---

### **Technical Skills**

- **Programming Languages**: Python (Expert), PHP (Advanced), TypeScript (Intermediate), Golang (Beginner), Dart/Flutter
- **Cloud & DevOps**: AWS, Docker, CI/CD, Linux, Kubernetes
- **Software Development**: TDD, BDD, AI-Assisted Coding, API Development
- **Project Management & Leadership**: Scrum, Kanban, Agile, Team Leadership

---

### **Certifications**

- **AWS Certified [Specify Exact Certification]**
- **Big Data & Data Fundamentals Certification**

---

### **Work Experience**

#### **Senior Software Engineer** | Above Surveying | **Feb 2023 – Present (Remote)**  
- Led the **backend infrastructure overhaul**, increasing system performance by **40%**.
- Built a **new digital twin system**, managing the **largest solar panel health database** in the world.
- Optimized API architecture, **reducing response time by 35%**.

#### **Senior Software Engineer** | Pooldata | **Sep 2022 – Jan 2023 (Remote)**  
- Worked on **data marketplace development** using TypeScript.
- Implemented **data pipeline optimizations**, improving query efficiency.

#### **Senior Software Engineer** | XIM - Lifelight | **Oct 2021 – Sep 2022 (Remote)**  
- Developed **ETL pipelines** for the science team, enabling **quick and easy data access**.
- Detected **irregularities in fundamental datasets**, improving data integrity.

#### **Team Lead** | Avanti Communications | **Mar 2019 – May 2021 (Remote)**  
- Led a team using **Extreme Programming and Scrum** in a waterfall-based corporation.
- Introduced **Scrum methodologies and team process improvements**.

#### **Previous Experience Includes:**  
- **Senior Engineer** roles at Reward Gateway, Browser, and Liberty Tech.
- **Game Developer** as a Sole Trader.
- **Software Engineer (Contractor)** at various companies in the UK and Portugal.

---

### **Projects & Contributions**

- **GitHub & Open Source Contributions**
- **Notable Freelance Work**: Developed scalable API systems for startups and businesses.
- **Automation & DevOps**: Implemented automated CI/CD pipelines and cloud infrastructure solutions.

---

### **Education**

- **Zend PHP 5.3**, London, UK – **2011**
- **University degree in Advertising**, UFP Porto, Portugal – **2005**

---

### **Contact & Availability**

- Best reached via **email (rodrigo@rodderscode.co.uk)**.
- Available for **remote work** with **4+ hours overlap** with US time zones.
- More details at: [rodderscode.co.uk](http://rodderscode.co.uk)

---


