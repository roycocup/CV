from . import symbols
from . import helpers

def get_size_chart(data, inc_percent=False):
    output = ''
    longest_word = max(len(k) for k in data.keys())

    for key, value in data.items():
        output += helpers.get_n_symbols(1, ' *') + helpers.get_n_spaces(1) + key
        output += helpers.get_n_spaces(longest_word - len(key) + 2)
        n = value // 10

        if n > 0:
            output += helpers.get_n_p_symbols(min(n, 3), symbols.filled_blocks()[0])
        if n > 3:
            output += helpers.get_n_p_symbols(min(n-3, 3), symbols.filled_blocks()[1])
        if n > 6:
            output += helpers.get_n_p_symbols(min(n-6, 3), symbols.filled_blocks()[2])

        if inc_percent:
            output += f"{value}%"

        output += "\n"

    return output 