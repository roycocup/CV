╔══════════════════╗
║ PERSONAL DETAILS ║
╚══════════════════╝
  Rodrigo Dias
  rodrigo@rodderscode.co.uk
  Remote

  

 * Python        ░░░▒▒▒▓▓▓
 * PHP           ░░░▒▒▒▓▓
 * Typescript    ░░░▒
 * Dart/Flutter  ░░░
 * Golang        ░░

╔══════════════╗
║ CERTIFICATES ║
╚══════════════╝
 * Big Data           ░░░▒▒▒▓▓▓
 * Data Fundamentals  ░░░▒▒▒▓▓
 * AWS                ░░░▒
 * SQL                ░░░

╔═════════════════╗
║ OTHER KNOWLEDGE ║
╚═════════════════╝
 * AI Assisted Coding  ░░░▒▒▒▓▓▓
 * Docker              ░░░▒▒▒▓▓
 * BDD/TDD             ░░░▒▒▒▓
 * AWS                 ░░░▒▒▒
 * Scrum               ░░░▒▒
 * CI/CD               ░░░▒
 * Linux               ░░░

 * Lead Developer     ░░░▒▒▒▓▓▓
 * Team Player        ░░░▒▒▒▓▓
 * Scrum Master       ░░░▒▒▒▓
 * Platform Engineer  ░░░▒▒▒
 * Client Contact     ░░░▒▒

╔═════════════════╗
║ WORK EXPERIENCE ║
╚═════════════════╝
+-------------------------+-----------------------+----------+---------------------+
| Title                   | Company               | Location | Dates               |
+-------------------------+-----------------------+----------+---------------------+
| Senior Engineer         | Above Surveying       | Remote   | Feb 2023 - Present  |
| Senior Engineer         | Pooldata              | Remote   | Sep 2022 - Jan 2023 |
| Senior Engineer         | XIM - Lifelight       | Remote   | Oct 2021 - Sep 2022 |
| Senior Engineer         | Reward Gateway        | Remote   | May 2021 - Aug 2021 |
| Team Lead               | Avanti Communications | Remote   | Mar 2019 - May 2021 |
| Senior Engineer         | Browser               | London   | Oct 2017 - Mar 2019 |
| Senior Engineer (Con)   | VGD                   | London   | Mar 2016 - Aug 2017 |
| Game Dev                | Sole Trader           | London   | Sep 2015 - Feb 2016 |
| Software Engineer (Con) | Liberty Tech          | London   | Mar 2014 - Aug 2015 |
| Software Engineer (Con) | Travel Joy            | London   | Aug 2013 - Mar 2014 |
| Software Enginner (Con) | Premier IT            | London   | Jul 2012 - Aug 2013 |
| Software Engineer (Con) | Pilotbean             | London   | Nov 2011 - Apr 2012 |
| PHP Developer (Con)     | Helastel              | Bristol  | Mar 2011 - Sep 2011 |
| PHP Developer           | CabinetUK             | London   | Jul 2008 - Aug 2010 |
| PHP Developer           | Markettiers 4DC       | London   | Jan 2008 - May 2008 |
| PHP Developer           | Migs On               | London   | Nov 2007 - Jan 2008 |
| Freelancer              | Frontspace            | Portugal | Apr 2005 - Jul 2007 |
| 3D Modeller             | Fullscreen            | Portugal | Jun 2000 - Apr 2004 |
+-------------------------+-----------------------+----------+---------------------+

  

╔═════════╗
║ SUMMARY ║
╚═════════╝
  * 18 years of experience | Multiple Industries | contractor and permanent. 

  * At Above I overhauled the backend infrastructure and built a brand new digital twin for the biggest solar panel healthiness database in the world.

  * At XIM I built an ETL for science team to enable quick and easy access and detected irregularities in fundamental datasets.

  * At Pooldata, picked up TypeScript to help the team build a data marketplace that would revolutionise the market.

  * I had a rewarding experience leading a team at Avanti Communications using Extreme Programming and Scrum inside a otherwise waterfall corporation.

  * In general was always responsible for bringing innovation to workplaces, such as the first Data Warehousing at Xim, Neo4j and Scrum at Avanti Communications, Go and Kanban at Browser, Git, Shell scripting at LibertyTech and Python automations at PremierIT.

  * Also... I really think that waterfall is preferable to bad Scrum.

╔══════════════════╗
║ PERSONAL DETAILS ║
╚══════════════════╝
  * I'm good at building smart applications involving multiple complex components, API's and third-party connections. I'm not very good at css, and generally take longer than your average developer.

  * I'm honest, reliable and very process/product focused. I like to review team and process topologies in order to align with business goals.

  * The easiest way to catch me is via my email (rodrigo@rodderscode.co.uk) as my phone is always on mute

  * If you are reading this on a computer, check out my website at http://rodderscode.co.uk for more details.

  * This CV was purposely made with ASCII so that it fits into every single upload form on the internet (and mess up recruiters standard software) and also because it looks "retro" and cool. Made with love using Python
