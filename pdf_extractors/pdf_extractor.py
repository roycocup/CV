
documents = [
    # "pdf_extractors/test_pdfs/CV2017GP.pdf",
    # "pdf_extractors/test_pdfs/cv_b_2023.pdf",
    # "pdf_extractors/test_pdfs/EM-2021.pdf",
    "pdf_extractors/test_pdfs/resume-2025.pdf"
]

def miner():
    from pdfminer.high_level import extract_text
    for document in documents:
        header(document)
        text = extract_text(document)
        print(text)

def fitz():
    import fitz
    for document in documents:
        header(document)
        doc = fitz.open(document)
        text = "\n".join(page.get_text() for page in doc)
        print(text)

def plumber():
    import pdfplumber
    for document in documents:
        header(document)
        text = ""
        with pdfplumber.open(document) as pdf:
            for page in pdf.pages:
                text += page.extract_text() + "\n"
        print(text)

def header(s):
        print("X"*80)
        print(s)
        print("X"*80)

def call_all():
    header("plumber")
    plumber()
    header("fitz")
    fitz()
    header("miner")
    miner()

if __name__ == "__main__":
    call_all()

 
