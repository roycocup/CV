from datetime import datetime
from .symbols import *
from .helpers import *
from .items_chart import *
from .boxed_title import *
from .text_line import *
from .data_columns import *
from .logo import *
from .circle_graph import * 
from .slider import *
from .switches import switches

class CVGenerator:
    def __init__(self):
        self.output = []
        self.column_buffer = []
        self.column_width = 40
        self.current_year = datetime.now().year
        self.switches = switches

    def generate_cv(self):
        email = "rod@rodderscode.co.uk"

        # Personal details
        if self.switches["personal"]:
            self.add_to_output(boxed_title("Personal Details"))
            self.add_to_output(text_line("Rodrigo Dias", space_below=False))
            self.add_to_output(text_line(email, space_below=False))
            self.add_to_output(text_line("Remote from London, UK"))

        # Languages
        if self.switches["languages"]:
            self.add_to_output(boxed_title("Programming Languages"))
            data = {
                "Python": 90,
                "PHP": 60,
                "Typescript": 40,
                "Dart/Flutter": 30,
                "Golang": 20,
                "JavaScript": 50,
            }
            self.add_to_output(get_size_chart(data))


        # Data Frameworks
        if self.switches["tools"]:
            self.add_to_output(boxed_title("Main Tools"))
            data = {
                "Amazon Web Services": 100,
                "SQL": 90,
                "Scrapy": 75,
                "FastAPI": 70,
                "Flask": 60,
                "Apache Airflow": 50,
                "Apache Spark": 40,
            }
            self.add_to_output(get_size_chart(data))
        
        # Other Knowledge
        if self.switches["other"]:
            self.add_to_output(boxed_title("Important Skills"))
            data = {
                "Large Codebase Coding": 100,
                "ETL": 90,
                "Linux Servers": 80,
                "Data Pipelines": 70,
                "Data Warehousing": 60,
                "CI/CD": 50,
            }
            self.add_to_output(get_size_chart(data))
        
        # Certificates
        if self.switches["certificates"]:
            self.add_to_output(boxed_title("Certificates"))
            data = [
                "AWS (in progress)",
                "Data Fundamentals (IBM)",
                "Big Data (IBM)",
                "Scrum Master (Scrum.org)",
                "PHP (zend)"
            ]
            for item in data:
                self.add_to_output(text_line(item, bullet=True, space_below=False))
            self.add_to_output("")


        # Secondary Roles
        if self.switches["roles"]:
            self.add_to_output(boxed_title("Roles"))
            data = {
                "Platform Engineer": 100,
                "Data Engineer": 90,
                "Senior Engineer": 80,
            }
            self.add_to_output(get_size_chart(data))
            
        if self.switches["skills_bias"]:
            self.add_to_output(boxed_title("Profile Bias"))
            self.add_to_output(slider(left_label="Intuitive", right_label="Theorical", skew=0.70))
            self.add_to_output(slider(left_label="Cloud", right_label="On-prem", skew=0.50))
            self.add_to_output(slider(left_label="Solo", right_label="Team", skew=0.70))
            self.add_to_output("")
            
        if self.switches["coding_bias"]:
            self.add_to_output(boxed_title("Coding Bias"))
            self.add_to_output(slider(left_label="Speed", right_label="Maintainability", skew=0.70))
            self.add_to_output(slider(left_label="Tidy", right_label="Messy", skew=0.20))
            self.add_to_output(slider(left_label="Testing", right_label="Debugging", skew=0.10))
            self.add_to_output("")

        self.flush_columns()

        # Circle Graph
        if self.switches["circle"]:
            data = {
                "radius": 10,
                "data": [{"javascript": 25, "sql": 25, "php": 25, "others": 25}]
            }
            c = CircleGraph(data)
            self.add_to_output(c.draw())

        self.add_to_output("<<<PAGE_BREAK>>>")

        # Work Experience
        if self.switches["columns"]:
            self.add_to_output(boxed_title("Work Experience"))
            data = [
                ["Title", "Company", "Location", "Dates"],
                ["Senior Engineer", "Above Surveying", "Remote", "Feb 2023 - Present"],
                ["Senior Engineer", "Pooldata", "Remote", "Sep 2022 - Jan 2023"],
                ["Senior Data Engineer", "XIM - Lifelight", "Remote", "Oct 2021 - Sep 2022"],
                ["Lead Engineer", "Reward Gateway", "Remote", "May 2021 - Aug 2021"],
                ["Lead Engineer", "Avanti Communications", "Remote", "Mar 2019 - May 2021"],
                ["Senior Engineer", "Browser", "London", "Oct 2017 - Mar 2019"],
                ["Senior Engineer (Con)", "VGD", "London", "Mar 2016 - Aug 2017"],
                ["Game Dev", "Sole Trader", "London", "Sep 2015 - Feb 2016"],
                ["Software Engineer (Con)", "Liberty Tech", "London", "Mar 2014 - Aug 2015"],
                ["Software Engineer (Con)", "Travel Joy", "London", "Aug 2013 - Mar 2014"],
                ["Software Enginner (Con)", "Premier IT", "London", "Jul 2012 - Aug 2013"],
                ["Software Engineer (Con)", "Pilotbean", "London", "Nov 2011 - Apr 2012"],
                ["PHP Developer (Con)", "Helastel", "Bristol", "Mar 2011 - Sep 2011"],
                ["PHP Developer", "CabinetUK", "London", "Jul 2008 - Aug 2010"],
                ["PHP Developer", "Markettiers 4DC", "London", "Jan 2008 - May 2008"],
                ["PHP Developer", "Migs On", "London", "Nov 2007 - Jan 2008"],
                ["Freelancer", "Frontspace", "Portugal", "Apr 2005 - Jul 2007"],
                ["3D Modeller", "Fullscreen", "Portugal", "Jun 2000 - Apr 2004"],
            ]
            self.add_to_output(data_columns(data))
            

        if self.switches["extra"]:
            self.add_to_output(text_line("I always bring innovation to the workplace", True))
            self.add_to_output(text_line("At Above I overhauled the backend infrastructure and built a brand new digital twin for the biggest solar panel healthiness database in the world.", True))
            self.add_to_output(text_line("At XIM I've built an ETL for science team to enable quick and easy access and detected irregularities in fundamental datasets.", True))
            self.add_to_output(text_line("I used Extreme Programming and Scrum inside a otherwise waterfall corporation at Avanti Communications.", True))
            self.add_to_output(text_line("Always bringing innovation to workplaces, such as the first Data Warehousing at Xim, Neo4j and Scrum at Avanti Communications, Go and Kanban at Browser, Git, Shell scripting at LibertyTech and Python automations at PremierIT.", True))
            self.add_to_output(text_line("Also... I really think waterfall is preferable to bad Scrum.", True))

 
        # Overview
        if self.switches["overview"]:
            start_date = datetime(2007, 1, 1)
            years = datetime.now().year - start_date.year
            self.add_to_output(boxed_title("Summary"))
            self.add_to_output(text_line(f"{years} years of experience in many industries, with a focus on data engineering and backend development.", True))
            self.add_to_output(text_line("Track record of bringing innovation to every workplace", True))
            self.add_to_output(text_line("Big fan of Extreme Programming and Scrum.", True))
            self.add_to_output(text_line("Thinks waterfall is preferable to bad Scrum.", True))

        # Notes
        if self.switches["notes"]:
            self.add_to_output(boxed_title("Personal Details"))
            self.add_to_output(text_line("I use AI for speed, quality, debugging, creativity, documentation, whilst understanding its limitations and manually fixing flawed AI-generated code.", True))
            self.add_to_output(text_line("I'm very good at building smart applications involving multiple complex components, API's and third-party connections.", True))
            self.add_to_output(text_line("I'm honest, reliable and very product focused. I always like to align my process with business goals.", True))
            self.add_to_output(text_line(f"The easiest way to catch me is via my email ({email})", True))
            self.add_to_output(text_line("If you are reading this on a computer, check out my website at https://rodderscode.co.uk for more details.", True))
            self.add_to_output(text_line("This CV was purposely made with ASCII so that it fits into every single upload form on the internet (and mess up recruiters standard software) and also because it looks \"retro\" and cool.", True))

        # Core Data Engineering Skills
        if self.switches.get("core_data_engineering_skills", True):
            self.add_to_output(boxed_title("Core Data Engineering Skills"))
            skills = [
                "ETL Development & Feature Engineering: Built and optimized ETL pipelines using Python, PySpark, AWS Glue, Apache Airflow, Talend, and Hadoop.",
                "Data Warehousing & Storage: Designed and developed data warehouses, data marts, and lakehouses using AWS Redshift, Snowflake, PostgreSQL, MySQL, SAS, NoSQL, and Oracle Exadata.",
                "Big Data Processing: Experience with Apache Spark, Hive, Pig, Mahout, and Databricks for large-scale data processing and transformation.",
                "Data Pipelines & Workflow Orchestration: Implemented dynamic ETL pipelines in AWS, Azure, and GCP."
            ]
            for skill in skills:
                self.add_to_output(text_line(skill, bullet=True, space_below=False))
            self.add_to_output("")

        # Cloud & Infrastructure Management
        if self.switches.get("cloud_infrastructure_management", True):
            self.add_to_output(boxed_title("Cloud & Infrastructure Management"))
            skills = [
                "Cloud Platforms: Extensive experience in AWS, Azure, and GCP.",
                "Cloud Cost Optimization: Managed AWS EMR clusters, cloud billing, and resource allocation.",
                "Automation & DevOps: Used Jenkins, Ansible, Kubernetes, Terraform, and Airflow."
            ]
            for skill in skills:
                self.add_to_output(text_line(skill, bullet=True, space_below=False))
            self.add_to_output("")

        # Backend & API Development
        if self.switches.get("backend_api_development", True):
            self.add_to_output(boxed_title("Backend & API Development"))
            skills = [
                "Data API Development: Built APIs for data ingestion and retrieval using FastAPI and Flask.",
                "Asynchronous Processing: Leveraged asyncio for backend data processing.",
                "Backend Systems Integration: Ensured seamless data flow between backend applications and data platforms."
            ]
            for skill in skills:
                self.add_to_output(text_line(skill, bullet=True, space_below=False))
            self.add_to_output("")

        # Machine Learning & AI Integration
        if self.switches.get("ml_ai_integration", True):
            self.add_to_output(boxed_title("Machine Learning & AI Integration"))
            skills = [
                "Feature Engineering for ML: Designed feature marts and customer 360° data lakes.",
                "Recommendation Engines: Contributed to Spark ML-based product recommendation systems.",
                "Advanced ML & AI: Applied GPT-4 Turbo for unstructured data analysis."
            ]
            for skill in skills:
                self.add_to_output(text_line(skill, bullet=True, space_below=False))
            self.add_to_output("")

        # Data Quality, Monitoring & Compliance
        if self.switches.get("data_quality_monitoring_compliance", True):
            self.add_to_output(boxed_title("Data Quality, Monitoring & Compliance"))
            skills = [
                "Data Quality & Governance: Developed data quality assessment tools in PySpark.",
                "Regulatory Reporting & Risk Management: Applied actuarial knowledge for compliance.",
                "SLA & Monitoring: Maintained 24x7x365 operations with strict production SLAs."
            ]
            for skill in skills:
                self.add_to_output(text_line(skill, bullet=True, space_below=False))
            self.add_to_output("")

        # Business Intelligence & Visualization
        if self.switches.get("business_intelligence_visualization", True):
            self.add_to_output(boxed_title("Business Intelligence & Visualization"))
            skills = [
                "BI & Reporting: Created interactive dashboards using Power BI, Plotly/Dash.",
                "Competitor Intelligence & Market Insights: Developed web scraping solutions.",
                "Retail & E-Commerce Analytics: Automated pricing and cataloging workflows."
            ]
            for skill in skills:
                self.add_to_output(text_line(skill, bullet=True, space_below=False))
            self.add_to_output("")


        return '\n'.join(self.output)
    

    def add_to_output(self, str_content):
        self.output.append(str_content)

    def add_to_column(self, str_content):
        self.column_buffer.append(str_content.split('\n'))

    def flush_columns(self):
        if not self.column_buffer:
            return
        
        max_height = max(len(block) for block in self.column_buffer)
        
        for i in range(max_height):
            row = ''
            for block in self.column_buffer:
                content = (block[i] if i < len(block) else '').ljust(self.column_width)
                row += content
            self.add_to_output(row)
        
        self.column_buffer = []
        self.add_to_output('')

    def calc_my_age(self):
        dob = datetime(1976, 10, 10)
        diff = datetime.now() - dob
        return int(diff.days / 365.25)
